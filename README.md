# [kotik.org](https://kotik.org) source codes

<br/>

### Run kotik.org on localhost

    # vi /etc/systemd/system/kotik.org.service

Insert code from kotik.org.service

    # systemctl enable kotik.org.service
    # systemctl start kotik.org.service
    # systemctl status kotik.org.service

http://localhost:4030
